## Name
A Tale of Two Cities processing and parsing practice.

## Description
Practicing using the NLTK library to parse some public domain literature for exploration. This project contains my Python code, the original text file, and my current results after processing. 


